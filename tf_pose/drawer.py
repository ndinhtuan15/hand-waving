import cv2
from tf_pose import common
import numpy as np
import math
import pyrealsense2 as rs

class Drawer:
    humans= []
    isVisitHuman = []
    wavingTimes = [{"Rhand": 0, "Lhand": 0} for i in range(4)]
    wavingTimeLimit = 2
    delta = 4
    numHumans = 0
    idHuman = -1
    isStart = True

    @staticmethod
    def getAverageX(human):
        sum = 0
        len = 0
        for i in range(common.CocoPart.Background.value):
            if i not in human.body_parts.keys():
                continue
            sum = sum + human.body_parts[i].x
            len = len + 1
        
        return sum / (1.0*len)

    @staticmethod
    def getAngleHuman(depth_frame, depthIntrin, human, image_w, image_h):
        sumAngle = 0
        count = 0
        for i in [0, 1, 2, 5, 8, 11, 14, 15, 16, 17]:
            if i not in human.body_parts.keys():
                continue
            body_part = human.body_parts[i]
            x = int(body_part.x * image_w + 0.5)
            y = int(body_part.y * image_h + 0.5)
            depth = depth_frame.get_distance(x, y)
            if(depth != 0):
                depth_point = rs.rs2_deproject_pixel_to_point(depthIntrin, [x, y], depth)
                degree = math.degrees(math.atan2(depth_point[0], depth_point[2]))
                sumAngle += degree
                count += 1
        if count == 0:
            return 0
        return int(sumAngle/count)


    @staticmethod
    def initHumanInfo(humans):
        numHumans = len(humans)
        Drawer.humans= [{
            "location": Drawer.getAverageX(human),
            "Rhand": {
                "minAngle": 0,
                "lastAngle": 0,
                "maxAngle": 0,
                "lastDiff": 0 
            },
            "Lhand": {
                "minAngle": 0,
                "lastAngle": 0,
                "maxAngle": 0,
                "lastDiff": 0
            }
        } for human in humans]
        Drawer.wavingTimes = [{"Rhand": 0, "Lhand": 0} for i in range(numHumans)]
        Drawer.idHuman = -1
    
    def getDistance(pointAx, pointBx):
        return abs(pointAx - pointBx)


    @staticmethod
    def mapHumanInfoWith(humans):
        hummanPosition = [human["location"] for human in Drawer.humans]
        hummanPositionClone = [human["location"] for human in Drawer.humans]
        isVisit = [False] * len(Drawer.humans)
        
            
        for human in humans:
            minDistance = 100000
            h_index = -1
            positionX = Drawer.getAverageX(human)
            for i, position in enumerate(hummanPosition):
                dis = Drawer.getDistance(positionX, position)
                if(dis < minDistance):
                    h_index = i
                    minDistance = dis
            
            if(h_index==-1):
                Drawer.initHumanInfo(humans)
                return
            
            if isVisit[h_index] and minDistance < Drawer.getDistance(hummanPositionClone[h_index], hummanPosition[h_index]):
                hummanPositionClone.append(hummanPositionClone[h_index])
                Drawer.humans.append({
                    "location": hummanPositionClone[h_index],
                    "Rhand": {
                        "minAngle": 0,
                        "lastAngle": 0,
                        "maxAngle": 0,
                        "lastDiff": 0 
                    },
                    "Lhand": {
                        "minAngle": 0,
                        "lastAngle": 0,
                        "maxAngle": 0,
                        "lastDiff": 0
                    }
                })
                isVisit.append(True)
                Drawer.wavingTimes.append({"Rhand": 0, "Lhand": 0})

            hummanPositionClone[h_index] = positionX
            Drawer.humans[h_index]["location"] = hummanPositionClone[h_index]
            isVisit[h_index] = True
        humanX = None

        if(Drawer.idHuman != -1):
            try:
                humanX = Drawer.humans[Drawer.idHuman] # code bug !!!!
            except IndexError:
                return
        Drawer.humans = [human for i, human in enumerate(Drawer.humans) if isVisit[i] == True]
        Drawer.wavingTimes = [wavingTime for i, wavingTime in enumerate(Drawer.wavingTimes) if isVisit[i] == True]
        if(Drawer.idHuman != -1):
            for i, human in enumerate(Drawer.humans):
                if (humanX["location"] == human["location"]):
                    Drawer.idHuman = i
                    return



    @staticmethod
    def findIndexHuman(human):
        positionX = Drawer.getAverageX(human)
        for i, prevHuman in enumerate(Drawer.humans):
            if positionX == prevHuman["location"]:
                return i
        return -1

    @staticmethod
    def resetHuman(h_index, hand):
        Drawer.humans[h_index][hand] = {
            "minAngle": 0,
            "lastAngle": 0,
            "maxAngle": 0,
            "lastDiff": 0 
        }
        Drawer.wavingTimes[h_index][hand] = 0
    
    @staticmethod
    def findBorderBox(human, image_w, image_h):
        topLeft_x = 10000
        topLeft_y = 10000
        bottomRight_x = 0
        bottomRight_y = 0

        for i in range(common.CocoPart.Background.value):
            if i not in human.body_parts.keys():
                continue
            body_part = human.body_parts[i]
            if topLeft_x > body_part.x:
                topLeft_x = body_part.x
            if topLeft_y > body_part.y:
                topLeft_y = body_part.y
            if bottomRight_x < body_part.x:
                bottomRight_x = body_part.x
            if bottomRight_y < body_part.y:
                bottomRight_y = body_part.y

        expandX = (bottomRight_x - topLeft_x)/6
        expandY = (bottomRight_y - topLeft_y)/6
        topLeft_x = topLeft_x-expandX
        bottomRight_x = bottomRight_x+expandX
        topLeft_y = topLeft_y-expandY
        bottomRight_y = bottomRight_y + expandY
        if topLeft_x-expandX < 0:
            topLeft_x = 0
        if bottomRight_x+expandX > 1:
            bottomRight_x = 1
        if topLeft_y-expandY < 0:
            topLeft_y = 0
        if bottomRight_y+expandY > 1:
            bottomRight_y = 1

        topLeft = (int(topLeft_x * image_w + 0.5), int(topLeft_y * image_h + 0.5))
        bottomRight = (int(bottomRight_x * image_w + 0.5), int(bottomRight_y * image_h + 0.5))
        print(bottomRight)
        return {"topLeft": topLeft, "bottomRight": bottomRight}

    @staticmethod
    def calculateDegree(elow, wrist, centers):
        x = centers[elow][0]-centers[wrist][0]
        y = centers[elow][1]-centers[wrist][1]
        degree = math.degrees(math.atan2(y, x))
        return degree

    @staticmethod
    def drawAngleOf(npimg, h_index, human, image_w, image_h):

        x = int((Drawer.getAverageX(human)) * image_w + 0.5)
        y = 100
        cv2.line(npimg, (x, 0), (x, image_h), (0,255,0), 2)
        cv2.putText(npimg, "L: %i" %Drawer.wavingTimes[h_index]["Lhand"], (x-30, y), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.putText(npimg, "R: %i" %Drawer.wavingTimes[h_index]["Rhand"], (x-30, y+20), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.putText(npimg, "AL: %i" %Drawer.humans[h_index]["Lhand"]["lastAngle"], (x-30, y+40), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.putText(npimg, "AR: %i" %Drawer.humans[h_index]["Rhand"]["lastAngle"], (x-30, y+60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.putText(npimg, "DL: %i" %Drawer.humans[h_index]["Lhand"]["lastDiff"], (x-30, y+80), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        cv2.putText(npimg, "DR: %i" %Drawer.humans[h_index]["Rhand"]["lastDiff"], (x-30, y+100), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

    @staticmethod
    def draw_arms(npimg, depth_frame, depthIntrin, humans, imgcopy=False):
        
        THRESH_ANGLE = 4
        human_rect = None
        avgX = None
        if imgcopy:
            npimg = np.copy(npimg)
        image_h, image_w = npimg.shape[:2]
        centers = {}
        cv2.putText(npimg, "Num people: %i" % len(humans), (image_w-300, 10),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

        if Drawer.isStart:
            Drawer.initHumanInfo(humans)
            Drawer.isStart = False
            return npimg, human_rect, None
        Drawer.mapHumanInfoWith(humans)

        for human in humans:
            h_index = Drawer.findIndexHuman(human)

            if Drawer.idHuman != -1 and Drawer.idHuman == h_index:
                # cv2.putText(npimg, "Hand Waving", (image_w-150, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                human_rect = Drawer.findBorderBox(human,image_w, image_h)
                avgX = int((Drawer.getAverageX(human)) * image_w + 0.5)
                Drawer.wavingTimes[h_index]["Rhand"] = 0 
                Drawer.wavingTimes[h_index]["Lhand"] = 0
                Drawer.idHuman = -1
                #angle = Drawer.getAngleHuman(depth_frame, depthIntrin, human, image_w, image_h)
                #cv2.putText(npimg, "Angle: %i" %angle, (image_w-150, 60), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                cv2.rectangle(npimg,human_rect["topLeft"],human_rect["bottomRight"],(0,255,0),3)
        
            if(h_index == -1):
                cv2.putText(npimg, "Error", (image_w-300, 30),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                return npimg, None, None
            # draw point
            for i in common.ArmsParts:
                if i not in human.body_parts.keys():

                    #Drawer.wavingTimes[h_index]["Rhand"] = 0 
                    #Drawer.wavingTimes[h_index]["Lhand"] = 0
                    #Drawer.idHuman = -1
                    #return npimg, None, None
                    continue
	
                body_part = human.body_parts[i]
                center = (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5))
                centers[i] = center
                cv2.circle(npimg, center, 3, common.CocoColors[i], thickness=3, lineType=8, shift=0)
                cv2.putText(npimg, "%i" %h_index, (int(body_part.x * image_w + 0.5), int(body_part.y * image_h + 0.5)-10), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

            Drawer.drawAngleOf(npimg, h_index, human, image_w, image_h)
            # draw line
            num_lines = 0 # expected : 4
            fake_arm = False
            for pair_order, pair in enumerate(common.ArmsPairs):
                if pair[0] not in human.body_parts.keys() or pair[1] not in human.body_parts.keys():
                    continue
                if pair in common.ForeArms:
                    degree = Drawer.calculateDegree(pair[0], pair[1], centers)

                    #draw angle of arms
                    if pair == common.RForeArms:
                        cv2.putText(npimg, "Degree right %i" %degree, (image_w-150, 70), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                    if pair == common.LForeArms:
                        cv2.putText(npimg, "Degree left %i" %degree, (image_w-150, 90), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                    if degree < 20 or degree > 160:
                        #print("Need reset humannnnnnnnnnnnnnnnnnnnnnnnnnn")
                        #Drawer.idHuman = -1
                        if pair == common.RForeArms:
                            Drawer.resetHuman(h_index, "Rhand")
                        elif pair == common.LForeArms:
                            Drawer.resetHuman(h_index, "Lhand")
                    else: 
                        if pair == common.RForeArms:
                            rHand = Drawer.humans[h_index]["Rhand"]
                            diff_angle = degree - rHand["lastAngle"]

                            if degree > 20 and degree < 160 and abs(diff_angle) > Drawer.delta:
                                if rHand["lastDiff"] * diff_angle == 0:
                                    rHand["minAngle"] = degree
                                elif rHand["lastDiff"] * diff_angle < 0:
                                    if diff_angle < 0 and rHand["maxAngle"] < degree:
                                        rHand["maxAngle"] = degree
                                    if diff_angle > 0 and rHand["minAngle"] > degree:
                                        rHand["minAngle"] = degree
                                    
                                    #if(rHand["maxAngle"] != 0 and rHand["minAngle"] != 0 and abs(rHand["maxAngle"] - rHand["minAngle"]) > THRESH_ANGLE):
                                    if abs(rHand["maxAngle"] - rHand["minAngle"]) > THRESH_ANGLE:

                                        Drawer.wavingTimes[h_index]["Rhand"] += 1
                                        if(Drawer.wavingTimes[h_index]["Rhand"] >= Drawer.wavingTimeLimit and Drawer.idHuman == -1):
                                            Drawer.idHuman = h_index
                                            cv2.putText(npimg, "Index %i" %h_index, (image_w-300, 50),  cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
                                        cv2.putText(npimg, "WavingTime %i" %Drawer.wavingTimes[h_index]["Rhand"], (image_w-150, 110), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                                rHand["lastDiff"] = diff_angle
                            rHand["lastAngle"] = degree
                        
                        if pair == common.LForeArms:
                            lHand = Drawer.humans[h_index]["Lhand"]
                            diff_angle = degree - lHand["lastAngle"]

                            if degree > 20 and degree < 160 and abs(diff_angle) > Drawer.delta:
                                if lHand["lastDiff"] * diff_angle == 0:
                                    lHand["minAngle"] = degree
                                elif lHand["lastDiff"] * diff_angle < 0:
                                    if diff_angle < 0 and lHand["maxAngle"] < degree:
                                        lHand["maxAngle"] = degree
                                    if diff_angle > 0 and lHand["minAngle"] > degree:
                                        lHand["minAngle"] = degree
                                    
                                    #if(lHand["maxAngle"] != 0 and lHand["minAngle"] != 0 and (lHand["maxAngle"] - lHand["minAngle"]) > THRESH_ANGLE):
                                    if abs(lHand["maxAngle"] - lHand["minAngle"]) > THRESH_ANGLE:
                                        Drawer.wavingTimes[h_index]["Lhand"] += 1
                                        if(Drawer.wavingTimes[h_index]["Lhand"] >= Drawer.wavingTimeLimit and Drawer.idHuman == -1):
                                            Drawer.idHuman = h_index
                                        cv2.putText(npimg, "WavingTime %i" %Drawer.wavingTimes[h_index]["Lhand"], (image_w-150, 130), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)

                                lHand["lastDiff"] = diff_angle
                            lHand["lastAngle"] = degree

                cv2.line(npimg, centers[pair[0]], centers[pair[1]], common.CocoColors[pair_order], 3)

                #if human_rect is not None:
                #    print("Heheh: {}".format(abs(centers[pair[0]][1] - centers[pair[1]][1])))

                num_lines += 1

                if abs(centers[pair[0]][1] - centers[pair[1]][1]) < 15:

                    fake_arm = True

            #if fake_arm :
            #    Drawer.wavingTimes[h_index]["Lhand"] = 0
            #    Drawer.wavingTimes[h_index]["Rhand"] = 0

            #    if Drawer.idHuman == h_index:
            #        human_rect = None
            #        avgX = None
            #    break
            #
            #print("num_lines: {} {}".format(num_lines, Drawer.idHuman))
            if (num_lines < 4 or fake_arm) and Drawer.idHuman == h_index:
                Drawer.idHuman = -1
                Drawer.wavingTimes[h_index]["Lhand"] = 0
                Drawer.wavingTimes[h_index]["Rhand"] = 0
                return npimg, None, None
            #else:
            #    print("Waving person: Drawer.idHuman: {} - h_index: {}".format(Drawer.idHuman, h_index))

        cv2.putText(npimg, "ID %i" % Drawer.idHuman, (image_w-300, 50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        #print("dddddddddddddddddddddddddddd", human_rect)
        return npimg, human_rect, avgX
